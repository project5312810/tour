import React from 'react'
import './Trip.css'
import TripData from './TripData'
import Trip1 from '../../assets/5.jpg'
import Trip2 from '../../assets/6.jpg'
import Trip3 from '../../assets/8.jpg'
const Trip = () => {
    return (
        <div className='trip'>
            <h1>Recent Trips</h1>
            <p>You can discover unique destinations using Google Maps.</p>
            <div className='trip-card'>
                <TripData
                    image={Trip1}
                    heading='Trip in Indonesia'
                    text='Indonesia is a nation of staggering statistics; as the fourth most populous country in the world, 
                    it’s made up of 8,000 inhabited islands (more than 17,000 across the entire archipelago) and over 300 
                    different native languages spoken across them. The end result is an enchanting melting pot of cultures, 
                    cuisines and dramatic landscapes. As the world’s largest archipelago, Indonesia is fully stocked with 
                    mesmerising natural sights.'
                />
                <TripData
                    image={Trip2}
                    heading='Trip in France'
                    text='If you’re planning a trip to France, you will want to read this France Travel Planner. 
                    This France Trip Planner covers everything you need to know to plan the ultimate trip, 
                    including the best places to visit in France, all the best things to do in France, 
                    the best time to visit France, whether you need a visa, and more.'
                />
                <TripData
                    image={Trip3}
                    heading='Trip in Malaysia'
                    text='Malaysia is a popular travel destination but when many visitors think about where to visit in Malaysia, 
                    they settle for the big city of Kuala Lumpur and the beautiful beaches in Penang and on the island of Langkawi, 
                    leaving so many other best cities in Malaysia to visit.'
                />
            </div>
        </div>
    )
}

export default Trip