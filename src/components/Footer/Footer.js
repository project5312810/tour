import React from 'react'
import { AiFillFacebook, AiFillInstagram, AiFillSkype, AiFillTwitterCircle } from 'react-icons/ai'
import './Footer.css'
const Footer = () => {
  return (
    <div className='footer'>
      <div className='top'>
        <div>
          <h1>Trippy</h1>
          <p>Chosse your favourite destinations</p>
        </div>
        <div>
          <a href='/'>
            <i><AiFillFacebook /></i>
          </a>
          <a href='/'>
            <i><AiFillInstagram /></i>
          </a>
          <a href='/'>
            <i><AiFillTwitterCircle /></i>
          </a>
          <a href='/'>
            <i><AiFillSkype /></i>
          </a>
        </div>
      </div>
      <div className='bottom'>
        <div>
          <h4>Project</h4>
          <a href='/'>Changelog</a>
          <a href='/'>Status</a>
          <a href='/'>License</a>
          <a href='/'>All Versions</a>
        </div>
        <div>
          <h4>Community</h4>
          <a href='/'>GitHub</a>
          <a href='/'>Issues</a>
          <a href='/'>Project</a>
          <a href='/'>Twitter</a>
        </div>
        <div>
          <h4>Help</h4>
          <a href='/'>Support</a>
          <a href='/'>Troubleshooting</a>
          <a href='/'>Contact Us</a>
        </div>
        <div>
          <h4>Others</h4>
          <a href='/'>terms of Service</a>
          <a href='/'>Privacy Policy</a>
          <a href='/'>License</a>
        </div>
      </div>
    </div>
  )
}

export default Footer