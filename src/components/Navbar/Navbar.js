import React, { Component } from 'react'
import './Navbar.css'
import { MenuItems } from '../MenuItems/MenuItems'
import { FaBars, FaTimes } from 'react-icons/fa'
import { Link } from 'react-router-dom'
class Navbar extends Component {
    state = { clicked: false }
    handleClick = () => {
        this.setState({ clicked: !this.state.clicked })
    }
    render() {
        return (
            <nav className='navbarItems'>
                <h1 className='navbar-logo'>Trippy</h1>
                <div className='menu-icons' onClick={this.handleClick}>
                    <i>{this.state.clicked ? <FaTimes /> : <FaBars />}</i>
                </div>
                <ul className={this.state.clicked ? "nav-menu active" : "nav-menu"}>
                    {MenuItems.map((item, index) => {
                        return (
                            <li key={index}>
                                <Link className={item.name} to={item.url}>
                                    <i>{item.icon}</i>
                                    {item.title}
                                </Link>
                            </li>
                        );
                    })}
                    <button>Sign Up</button>
                </ul>
            </nav>
        )
    }

}

export default Navbar