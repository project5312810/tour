import React from 'react'
import './AboutUs.css'
const AboutUs = () => {
    return (
        <div className='about-container'>
            <h1>Our History</h1>
            <p>This tool helps you figure out where to stay if you're visiting a new destination.
                The suggestions are based on feedback from Trippy members,
                and includes neighborhoods as well as the best hotels that are centrally located and
                convenient for travelers. Most locals know their area well, so we gather all the best
                tips and suggestions to help people who are new to the area.
            </p>
            <h1>Our Mission</h1>
            <p>Once you have some ideas of where to stay, you can choose the best option based on your
                own personal preferences, like whether you prefer to stay downtown close to the main tourist
                attractions, or you want something different that gives you more of a local flavor.
                You may prefer a hotel with a view or within walking distance of shops and restaurants,
                or you might want to stay in an Airbnb or vacation rental to explore the area for a longer time.
                Either way, you can get ideas on the best places to stay from locals who live in the area and post
                questions on Trippy to get even more help with your next trip!
            </p>
            <h1>Our Vision</h1>
            <p>The Trippy flight planner helps you look for the best airports to fly between cities,
                and calculates your true flight time including all the extra time to get to the airport,
                go through security, arrive at the gate, and taxi on the runway. This gives you a better
                way to plan your travel day.
            </p>
        </div>
    )
}

export default AboutUs