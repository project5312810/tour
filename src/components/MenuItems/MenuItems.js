import { AiOutlineHome, AiOutlineInfoCircle } from "react-icons/ai";
import { BsBriefcase } from "react-icons/bs";
import { FaRegAddressCard } from "react-icons/fa";
import { BiLogIn } from "react-icons/bi";
export const MenuItems = [
    {
        title: "Home",
        url: "/",
        name: "nav-links",
        icon: <AiOutlineHome />
    },
    {
        title: "About",
        url: "/about",
        name: "nav-links",
        icon: <AiOutlineInfoCircle />
    },
    {
        title: "Service",
        url: "/service",
        name: "nav-links",
        icon: <BsBriefcase />
    },
    {
        title: "Contact",
        url: "/contact",
        name: "nav-links",
        icon: <FaRegAddressCard />
    },
    {
        title: "Sign Up",
        url: "/signup",
        name: "nav-links-mobile",
        icon: <BiLogIn />
    },
];