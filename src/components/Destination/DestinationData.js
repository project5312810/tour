import React, { Component } from 'react'
import './Destination.css'

class DestinationData extends Component {
    render() {
        return (
            <>
                <div className={this.props.className}>
                    <div className='text-des'>
                        <h2>{this.props.heading}</h2>
                        <p>{this.props.text}</p>
                    </div>
                    <div className='image'>
                        <img alt='img' src={this.props.Img1} />
                        <img alt='img' src={this.props.Img2} />
                    </div>
                </div>
            </>
        )
    }
}

export default DestinationData