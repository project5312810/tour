import React from 'react'
import "./Destination.css"
import DestinationData from './DestinationData'
import Img1 from '../../assets/1.jpg'
import Img2 from '../../assets/2.jpg'
import Img3 from '../../assets/3.jpg'
import Img4 from '../../assets/4.jpg'
const Destination = () => {
    return (
        <div className='destination'>
            <h1>Popular Destinations</h1>
            <p>Tours give you the opportunity to see a lot, within a time frame.</p>
            <DestinationData
                className="first-des"
                heading="Taal Volcano, Batangas"
                text="Taal Volcano has had several violent eruptions in the past,
                causing deaths on the island and the populated areas surrounding the lake,
                with an overall death toll of about 6,000. Because of its proximity to populated areas and its eruptive history,
                the volcano was designated a Decade Volcano, worthy of close study to prevent future natural disasters.
                All volcanoes in the Philippines are part of the Ring of Fire."
                Img1={Img1}
                Img2={Img2}
            />
            <DestinationData
                className="first-des-reverse"
                heading="Mount Daguldol, Batangas"
                text="Explore this 8.9-km out-and-back trail near Lobo, 
                Batangas. Generally considered a challenging route, 
                it takes an average of 4 h 0 min to complete. 
                This is a popular trail for camping, hiking, and walking, 
                but you can still enjoy some solitude during quieter times of day."
                Img1={Img3}
                Img2={Img4}
            />
        </div>
    )
}

export default Destination