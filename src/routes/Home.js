import React from 'react'
import Hero from '../components/Hero/Hero'
import Navbar from '../components/Navbar/Navbar'
import Img12 from '../assets/12.jpg'
import Destination from '../components/Destination/Destination'
import Trip from '../components/Trip/Trip'
import Footer from '../components/Footer/Footer'
const Home = () => {
    return (
        <>
            <Navbar />
            <Hero
                cName="hero"
                heroImg={Img12}
                title="Your Journey Your Story"
                text="Choose Your Favourite Destination."
                buttonText="Travel Plan"
                url="/"
                btnClass="show"
            />
            <Destination />
            <Trip />
            <Footer />
        </>

    )
}

export default Home