import React from 'react'
import Hero from '../components/Hero/Hero'
import Navbar from '../components/Navbar/Navbar'
import ImgNight from '../assets/night.jpg'
import Footer from '../components/Footer/Footer'
import Trip from '../components/Trip/Trip'
const Service = () => {
    return (
        <>
            <Navbar />
            <Hero
                cName="hero-about"
                heroImg={ImgNight}
                title="Service"
                btnClass="service"
            />
            <Trip />
            <Footer />
        </>

    )
}

export default Service