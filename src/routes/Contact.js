import React from 'react'
import Hero from '../components/Hero/Hero'
import Navbar from '../components/Navbar/Navbar'
import ImgNight from '../assets/night.jpg'
import Footer from '../components/Footer/Footer'
import ContactForm from '../components/ContactForm/ContactForm'
const Contact = () => {
    return (
        <>
            <Navbar />
            <Hero
                cName="hero-about"
                heroImg={ImgNight}
                title="Contact"
                btnClass="contact"
            />
            <ContactForm />
            <Footer />
        </>

    )
}

export default Contact