import React from 'react'
import Hero from '../components/Hero/Hero'
import Navbar from '../components/Navbar/Navbar'
import ImgNight from '../assets/night.jpg'
import Footer from '../components/Footer/Footer'
import AboutUs from '../components/AboutUs/AboutUs'
const About = () => {
    return (
        <>
            <Navbar />
            <Hero
                cName="hero-about"
                heroImg={ImgNight}
                title="About"
                btnClass="about"
            />
            <AboutUs />
            <Footer />
        </>

    )
}

export default About